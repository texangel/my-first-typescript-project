/*  week 1 code

console.log("hello")

interface Car{
    Model: string;
    PurchasePrice: number;
    LotPrice: number;
    isInspected: boolean;
    isProfitable?: boolean
}

function isInspected(Car: Car): boolean{
    return Car.isInspected
}

function isProfitable(car: Car): boolean{
    return car.PurchasePrice < car.LotPrice
}

function sellable(car: Car): Boolean {
    return isProfitable && isInspected
}

let FordT: Car = {Model: "T", PurchasePrice: 10102987, LotPrice: 15400298, isInspected: false, isProfitable: true}
let ChevroletVar: Car = {Model: "Silverado", PurchasePrice: 10000, LotPrice: 10400, isInspected: true, isProfitable: true}
let TeslaY: Car = {Model: "Y", PurchasePrice: 50000, LotPrice: 60000, isInspected: true, isProfitable: true}
let HondaFit: Car = {Model: "Fit", PurchasePrice: 6000, LotPrice: 5300, isInspected: true, isProfitable: false}

console.log (FordT.Model);

console.log(`${FordT.Model} was purchased for ${FordT.PurchasePrice} although it was listed for ${FordT.LotPrice} because it is a show piece the vehicle ${FordT.isInspected}`)

let PriceMath = FordT.LotPrice - FordT.PurchasePrice;

console.log(`${PriceMath}`)


let FuncReturn = isInspected (HondaFit.isInspected);
console.log(`FuncReturn: ${FuncReturn}`)

let FuncReturn2 = isProfitable (HondaFit.isProfitable);
console.log(`FuncReturn2: ${FuncReturn2}`)

let FuncReturn3 = sellable (car.PurchasePrice, Car.LotPrice, HondaFit.isInspected);
console.log(`${HondaFit.Model} sellability is ${FuncReturn3}`)
*/


// Fibbonaci Sequence
function fibbonaciNumber(endnumber: number) {
    let numx: number = 0;
    let numy: number = 1;
    let total: number; 

    while (numy <= endnumber) {
        console.log(numx, numy);
        total = numy + numx;
        numx = numy;
        numy = total;
    }
}

fibbonaciNumber(987)

